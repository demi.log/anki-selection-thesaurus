from .models import DaijirinEntry, KojienEntry, MeiryoEntry, ShinmeikaiEntry


class DB:
    def lookup(self, query: str, dictionary: str, regexp=False, order=False):
        """
        Lookup selected query in dictionary table using ordering or without it.
        """
        model = self._get_model(dictionary)
        # TODO: add ordering options
        if order:
            return (
                model.select()
                .where(model.key.contains(query) | model.reading.contains(query))
                .order_by(model.reading)
            )
        else:
            return model.select().where(
                model.key.contains(query) | model.reading.contains(query)
            )

    def get(self, key: str, dictionary: str):
        model = self._get_model(dictionary)
        return model.get(model.key == key)

    def _get_model(self, dictionary: str):
        if dictionary == "meiryo":
            return MeiryoEntry
        elif dictionary == "daijirin":
            return DaijirinEntry
        elif dictionary == "shinmeikai":
            return ShinmeikaiEntry
        elif dictionary == "kojien":
            return KojienEntry
