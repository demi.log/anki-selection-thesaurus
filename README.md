# JP-JP thesaurus addon for Anki

![](thesaurus.gif)

## Idea

Select text and press `Alt+1` to show monolingual definition.

Press `Alt + 2|3|4` for other dictionaries.

Match by exact selection, then ordered by size.

Lookup in other sources with `Alt + 5..0`.

Lookup examples in Massif and Yourei, show results in tooltip via `Alt + l`.

Lookup YouGlish pronunciation via `Alt + p`.

## Details

Includes relatively large sqlite database and peewee, bs4, requests modules.
