from pathlib import Path
from peewee import AutoField, Model, SqliteDatabase, CharField, TextField


addon_dir = Path(__file__).parent.resolve()
db_path = addon_dir / "thesaurus.sqlite3"
db = SqliteDatabase(db_path)


class BaseModel(Model):
    class Meta:
        database = db

    def get_meanings_html(self) -> str:
        """
        Format dictionary entry.
        """
        m = self.meaning
        # Process situation when we don't have 【 (katakan only, for example)
        # e.g., Shinmeikai only has 【
        index = m.find("【")
        if index == 0:
            index = m.find("〘")

        # Highlight reading and grammatical part
        m = (
            "<span style='color: #ee9090;'>"
            + m[0:index]
            + "</span>"
            # + "<strong style='color: #9090ee;'>"
            + m[index:]
        )
        m = m.replace("〘", "<strong style='color: #9090ee;'>〘")
        m = m.replace("〙", "〙</strong>")
        m = m.replace("【", "<strong style='color: #9090ee;'>【")
        m = m.replace("】", "】</strong>")
        m = m.replace("\n", "<br/>")

        # Highlight numberings
        for n in [
            "①",
            "②",
            "③",
            "④",
            "⑤",
            "⑥",
            "⑦",
            "⑧",
            "⑨",
            "(一)",
            "(二)",
            "(三)",
            "(四)",
            "(五)",
        ]:
            m = m.replace(n, f"<span style='color: #90ee90;'>{n}</span> ")

        # Tone down content in the brackets
        m = m.replace("(", "<span style='color: #696969;'>(")
        m = m.replace(")", ")</span>")
        m = m.replace("〔", "<span style='color: #696969;'>〔")
        m = m.replace("〕", "〕</span>")
        m = m.replace("「", "<span style='color: #696969;'>「")
        m = m.replace("」", "」</span>")

        return m


class BaseEntry(BaseModel):
    id = AutoField(primary_key=True)
    key = CharField(index=True, max_length=255)
    reading = CharField(index=True, max_length=255, null=True)
    meaning = TextField(null=True)

    def __str__(self) -> str:
        return f"{self.id} {self.key} {self.reading} {self.meaning}"


class MeiryoEntry(BaseEntry):
    class Meta:
        table_name = "meiryo"


class DaijirinEntry(BaseEntry):
    class Meta:
        table_name = "daijirin"


class ShinmeikaiEntry(BaseEntry):
    class Meta:
        table_name = "shinmeikai"


class KojienEntry(BaseEntry):
    class Meta:
        table_name = "kojien"
