from aqt import mw


def get_config() -> dict:
    config: dict = mw.addonManager.getConfig(__name__) or dict()
    config["delay"] = int(config.get("delay", "10000"))
    config["x_offset"] = int(config.get("x_offset", "50"))
    config["thesaurus_offset"] = int(config.get("thesaurus_offset", "250"))
    config["example_offset"] = int(config.get("example_offset", "300"))

    return config


def write_config(config):
    for key in config.keys():
        if not isinstance(config[key], str):
            config[key] = str(config[key])
    mw.addonManager.writeConfig(__name__, config)


config = get_config()
