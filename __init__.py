import sys
from pathlib import Path
from random import choice, shuffle

from aqt import mw
from aqt.qt import QAction, QMenu
from aqt.utils import tooltip
from aqt.utils import showText
from aqt.utils import openLink

import requests
import bs4

# Insert external modules to system path (peewee)
addon_dir = Path(__file__).parent.resolve()
external_dir = addon_dir / "external"
sys.path.insert(0, str(external_dir))

from .lookup import DB
from .config import get_config

db = DB()
config = get_config()
AVAILABLE_SOURCES = ["meiryo", "shinmeikai", "kojien", "daijirin"]
LIMIT = 50
PICK = 2
HEADERS = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36"
}


class Lookup:
    """
    Lookup selected text in different JP-JP dictionaries.
    """

    MASSIF_URL = "https://massif.la/ja/search?q="
    IMMERSION_URL = "https://www.immersionkit.com/dictionary?keyword="
    IMMERSION_URL_V2 = "https://v2.immersionkit.com/search?keyword="
    KANSHUDO_URL = "https://www.kanshudo.com/searchq?q="
    KOTOBANK_URL = "https://kotobank.jp/gs/?q="
    KOTOBANK_URL = "https://kotobank.jp/word/"
    YOUREI_URL = "https://yourei.jp/"
    KANJIPEDIA_URL = (
        "https://www.kanjipedia.jp/search/?k={}&kt=1&wt=1&ky=1&wy=1&sk=partial&t=kotoba"
    )
    WEBLIO_URL = "https://thesaurus.weblio.jp/content/"
    WEBLIO_WORD_URL = "https://www.weblio.jp/content/"
    JPDB_URL = "https://jpdb.io/kanji/"
    YOUGLISH_URL = "https://youglish.com/pronounce/{}/japanese?"

    def __init__(self):
        pass

    def emphasize(self, text, item) -> str:
        return text.replace(item, f"<b>{item}</b>")

    def get_massif(self, query):
        if query == "":
            return ""
        # TODO: optionally 'query' for exact match
        r = requests.get(
            f"https://massif.la/ja/search?q={query}&fmt=json", headers=HEADERS
        )

        # Shuffle results
        results = r.json()["results"][:LIMIT]
        shuffle(results)
        results = results[:PICK]
        results = [r["text"] for r in results]
        text = "<br>".join(results)

        # Limit by N and get one from those
        # example = choice(r.json()["results"][:LIMIT])
        text = self.emphasize(text, query)
        return text

    def get_kotobank(self, query):
        url = f"https://kotobank.jp/word/{query}"
        res = requests.get(url, headers=HEADERS).text
        soup = bs4.BeautifulSoup(res, "html.parser")
        found = soup.find_all(attrs={"data-orgtag": "meaning"})

        if not found:
            # TODO: try "description" class
            ...

        if not found:
            return ""

        text = "<br/>".join([f.text for f in found])
        emphasized = self.emphasize(text, query)
        return emphasized

        # found = soup.find_all(attrs={"class":"description"})
        # for f in found:
        #     print(f.text)

    def get_yourei(self, query):
        r = requests.get(f"https://yourei.jp/{query}", headers=HEADERS)
        soup = bs4.BeautifulSoup(r.content, "html.parser")
        examples = []

        # Find all list elements with class "sentence"
        list_elements = soup.find_all("li", class_="sentence")
        for element in list_elements:
            sentence_element = element.find("span", class_="the-sentence")
            if sentence_element:
                examples.append(sentence_element.text)

        # Limit by N and get random from those
        if not examples:
            return ""
        text = choice(examples[:LIMIT])
        emphasized = self.emphasize(text, query)
        return emphasized

    def search_all(self, query):
        # TODO: show progressbar
        massif = self.get_massif(query)
        yourei = self.get_yourei(query)
        kotobank = self.get_kotobank(query)

        html = "<div style='font-size: 18px; font-family: Zen Maru Gothic, sans;'>"
        html += f"{massif}"
        if yourei:
            html += f"<br><br>{yourei}"
        if kotobank:
            # TODO: wrap text css
            html += f"<br><br>{kotobank}"

        html += "</div>"

        tooltip(
            html,
            period=config["delay"],
            y_offset=config["example_offset"],
            x_offset=config["x_offset"],
        )

    def selection(self, function):
        """Get the selected text and look it up with FUNCTION."""
        text = mw.web.selectedText()
        text = text.strip()
        # TODO: strip ruby|furigana?
        if not text:
            tooltip("Please select something!")
            return
        function(text)

    def thesaurus(self, text, thesaurus):
        """
        1. Query selected sqlite thesaurus
        2. Order by length
        3. Try to match exactly by selection
        4. Otherwise get the first found item
        """
        results = db.lookup(text, dictionary=thesaurus, regexp=False)
        if not results:
            tooltip("Nothing found T.T")
            return

        # Filter results and try to found exact match
        results = sorted(results, key=lambda item: len(item.key))

        item = None
        for r in results:
            if r.key == text:
                item = r

        if item is None:
            item = results[0]

        header_style = "font-size: 20px; font-family: Zen Maru Gothic, sans;"
        entry_style = "font-size: 18px; font-family: Zen Maru Gothic, sans;"
        html = f"""
        <h1 style="{header_style}"=>{item.key}</h1>
        """
        html += f"<p style={entry_style}>{item.get_meanings_html()}</p>"

        tooltip(
            html,
            period=config["delay"],
            x_offset=config["x_offset"],
            y_offset=config["thesaurus_offset"],
        )

    def meiryo(self, text):
        self.thesaurus(text, AVAILABLE_SOURCES[0])

    def shinmeikai(self, text):
        self.thesaurus(text, AVAILABLE_SOURCES[1])

    def kojien(self, text):
        self.thesaurus(text, AVAILABLE_SOURCES[2])

    def daijirin(self, text):
        self.thesaurus(text, AVAILABLE_SOURCES[3])

    def quick_lookup(self, text):
        self.search_all(text)

    def lookup_in_massif(self, text):
        url = self.MASSIF_URL + text
        openLink(url)

    def lookup_in_immersion(self, text):
        url = self.IMMERSION_URL_V2 + text
        openLink(url)

    def lookup_in_kanshudo(self, text):
        url = self.KANSHUDO_URL + text
        openLink(url)

    def lookup_in_yourei(self, text):
        url = self.YOUREI_URL + text
        openLink(url)

    def lookup_in_kotobank(self, text):
        url = self.KOTOBANK_URL + text
        openLink(url)

    def lookup_in_kanjipedia(self, text):
        if len(text) == 1:
            url = self.KANJIPEDIA_URL.format(text)
            openLink(url)

    def lookup_in_weblio(self, text):
        url = self.WEBLIO_URL + text
        openLink(url)

    def lookup_in_weblio_word(self, text):
        url = self.WEBLIO_WORD_URL + text
        openLink(url)

    def lookup_in_jpdb(self, text):
        url = self.JPDB_URL + text
        openLink(url)

    def youglish_lookup(self, text):
        url = self.YOUGLISH_URL.format(text)
        openLink(url)


def initLookup():
    """Lookup object may be None"""
    if not getattr(mw, "lookup", None):
        mw.lookup = Lookup()


def get_lookup_hook(source):
    """Generate callback for specified dictionary source"""

    def on_lookup():
        initLookup()
        mw.lookup.selection(getattr(mw.lookup, source))

    return on_lookup


def massif_hook():
    initLookup()
    mw.lookup.selection(mw.lookup.lookup_in_massif)


def immersion_hook():
    initLookup()
    mw.lookup.selection(mw.lookup.lookup_in_immersion)


def kanshudo_hook():
    initLookup()
    mw.lookup.selection(mw.lookup.lookup_in_kanshudo)


def quick_hook():
    initLookup()
    mw.lookup.selection(mw.lookup.quick_lookup)

def youglish_hook():
    initLookup()
    mw.lookup.selection(mw.lookup.youglish_lookup)


def everywhere_hook():
    initLookup()
    mw.lookup.selection(mw.lookup.lookup_in_immersion)
    mw.lookup.selection(mw.lookup.lookup_in_massif)
    mw.lookup.selection(mw.lookup.lookup_in_yourei)
    mw.lookup.selection(mw.lookup.lookup_in_kotobank)
    mw.lookup.selection(mw.lookup.lookup_in_weblio)
    mw.lookup.selection(mw.lookup.lookup_in_weblio_word)
    mw.lookup.selection(mw.lookup.lookup_in_kanjipedia)
    mw.lookup.selection(mw.lookup.lookup_in_jpdb)

def jpdb_hook():
    initLookup()
    mw.lookup.selection(mw.lookup.lookup_in_jpdb)

def kanjipedia_hook():
    initLookup()
    mw.lookup.selection(mw.lookup.lookup_in_kanjipedia)

def init():
    """
    Create thesaurus menu and populate it with lookup actions.
    Use all thesauruse from available sources.
    """
    ml = QMenu()
    ml.setTitle("JP-JP Thesaurus")
    mw.form.menuTools.addAction(ml.menuAction())
    mw.form.menuLookup = ml

    # Create action for each of the sources
    actions_list = []
    for i, source in enumerate(AVAILABLE_SOURCES):
        a = QAction(mw)
        a.setText(f"Lookup in {source}")
        a.setShortcut(f"Alt+{i+1}")
        ml.addAction(a)

        actions_list.append(a)

    # Assign callbacks for each action (using loop has problems, so...)
    callbacks_list = [get_lookup_hook(s) for s in AVAILABLE_SOURCES]

    actions_list[0].triggered.connect(lambda: callbacks_list[0]())
    actions_list[1].triggered.connect(lambda: callbacks_list[1]())
    actions_list[2].triggered.connect(lambda: callbacks_list[2]())
    actions_list[3].triggered.connect(lambda: callbacks_list[3]())

    # Create action for each of the sources
    action_jpdb = QAction(mw)
    action_jpdb.setText(f"Lookup in JPDB")
    action_jpdb.setShortcut(f"Alt+5")
    ml.addAction(action_jpdb)
    action_jpdb.triggered.connect(lambda: jpdb_hook())

    action_kanjipedia = QAction(mw)
    action_kanjipedia.setText(f"Lookup in Kanjipedia")
    action_kanjipedia.setShortcut(f"Alt+6")
    ml.addAction(action_kanjipedia)
    action_kanjipedia.triggered.connect(lambda: kanjipedia_hook())

    action_kanshudo = QAction(mw)
    action_kanshudo.setText(f"Lookup in Kanshudo")
    action_kanshudo.setShortcut(f"Alt+7")
    ml.addAction(action_kanshudo)
    action_kanshudo.triggered.connect(lambda: kanshudo_hook())

    action_massif = QAction(mw)
    action_massif.setText("Lookup in Massif")
    action_massif.setShortcut("Alt+8")
    ml.addAction(action_massif)
    action_massif.triggered.connect(lambda: massif_hook())

    action_immersion = QAction(mw)
    action_immersion.setText(f"Lookup in Immersion Kit")
    action_immersion.setShortcut(f"Alt+9")
    ml.addAction(action_immersion)
    action_immersion.triggered.connect(lambda: immersion_hook())

    action_everywhere = QAction(mw)
    action_everywhere.setText(f"Lookup Everywhere")
    action_everywhere.setShortcut(f"Alt+0")
    ml.addAction(action_everywhere)
    action_everywhere.triggered.connect(lambda: everywhere_hook())

    action_quick = QAction(mw)
    action_quick.setText(f"Lookup examples + kotobank")
    action_quick.setShortcut(f"Alt+l")
    ml.addAction(action_quick)
    action_quick.triggered.connect(lambda: quick_hook())

    action_quick = QAction(mw)
    action_quick.setText(f"Lookup pronunciation in YouGlish")
    action_quick.setShortcut(f"Alt+p")
    ml.addAction(action_quick)
    action_quick.triggered.connect(lambda: youglish_hook())


init()
